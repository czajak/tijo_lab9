package pl.edu.pwsztar;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Application {

    public static void main(String[] args) {
        SpringApplication.run(Application.class, args);
    }
}

// docs available here:
// https://app.swaggerhub.com/apis/dariuszczajka/MovieDB/1.0.0