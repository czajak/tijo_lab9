package pl.edu.pwsztar.domain.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import pl.edu.pwsztar.domain.entity.Movie;

import java.util.List;

@Repository
public interface MovieRepository extends JpaRepository<Movie, Long>, CrudRepository<Movie, Long> {

    @Modifying
    @Query("DELETE FROM Movie m WHERE m.movieId = :movieId")
    void deleteById(@Param("movieId") Long movieId);

    Movie findOneByMovieId(Long movieId);

    @Query("SELECT m FROM Movie m WHERE m.title LIKE %:movieTitle% ORDER BY m.year DESC ")
    List<Movie> findByOrderByYearDesc(@Param("movieTitle") String movieTitle);

    @Query("SELECT count(m) FROM Movie m WHERE m.title LIKE %:search%")
    Long count(@Param("search") String search);
}
